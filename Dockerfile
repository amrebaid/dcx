# Use Ubuntu as a parent image
FROM ubuntu

# Install packages
RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y python
RUN apt-get install -y python-pip
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
RUN apt-get install -y python-tk
RUN apt-get install -y graphviz
RUN apt-get install -y python-pydot

# Set the working directory to /dcx
WORKDIR /dcx

# Clone the code from BitBucket
# App Password: 7WLufzRCAQFst49xLuWu
RUN git clone https://amrebaid:7WLufzRCAQFst49xLuWu@bitbucket.org/amrebaid/dcx.git

COPY requirements.txt .

# Install python packages
#RUN pip install numpy
#RUN pip install scipy
#RUN pip install jupyter
#RUN pip install py_entitymatching
#RUN pip install lime
#RUN pip install anchor_exp
#RUN pip install plotly
#RUN pip install skater

#allow pip caching
RUN pip install --requirement ./requirements.txt

# Go to folder
WORKDIR /dcx/dcx

# Make port available to the world outside this container
EXPOSE 8000

# Run web2py when the container launches
CMD python web2py.py --nogui -a 'admin' -i 0.0.0.0
