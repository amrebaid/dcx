import json
import os

import py_entitymatching as em
import py_entitymatching.catalog.catalog_manager as cm

from er_explain import ERVectorizer


########################################################################################################################

# ER Dataset

class ERDataset:
    dataset_dir = ''
    name = ''
    dname1, dname2 = '', ''
    id1, id2 = '', ''

    def __init__(self, base_dir, random_state=None, n_jobs=-1):
        self.vectorizer = None

        self.base_dir = base_dir
        self.random_state = random_state
        self.n_jobs = n_jobs

    def load(self):
        path1 = os.path.join(self.base_dir, self.dataset_dir, self.dname1 + '.csv')
        df1 = em.read_csv_metadata(path1, **{'key': 'id', 'encoding': 'latin-1'})

        path2 = os.path.join(self.base_dir, self.dataset_dir, self.dname2 + '.csv')
        df2 = em.read_csv_metadata(path2, **{'key': 'id', 'encoding': 'latin-1'})

        # Set ids as indexes to allow accessing data frames later
        df1.set_index('id', drop=False, inplace=True)
        df2.set_index('id', drop=False, inplace=True)

        return df1, df2

    def block(self, df1, df2):
        block_path = os.path.join(self.base_dir, self.dataset_dir, 'blocks.csv')
        if os.path.isfile(block_path):
            bdf = em.read_csv_metadata(block_path, ltable=df1, rtable=df2, **{'encoding': 'latin-1'})
        else:
            bdf = self.reblock(df1, df2)
        return bdf

    def reblock(self, df1, df2):
        pass

    def label(self, df1, df2, cdf):
        # Read matches from file
        matches_path = os.path.join(self.base_dir, self.dataset_dir, 'matches.csv')
        mdf = em.read_csv_metadata(matches_path)

        matches = set()
        for m_row in mdf.itertuples():
            matches.add((getattr(m_row, self.id1), getattr(m_row, self.id2)))

        # Label candidates
        ldf = cdf.set_index('_id', drop=False).assign(gold=False)
        for l_row in ldf.itertuples():
            if (getattr(l_row, self.id1), getattr(l_row, self.id2)) in matches:
                ldf.at[l_row.Index, 'gold'] = True

        # Set candidate set properties
        cm.set_candset_properties(ldf, '_id', self.id1, self.id2, df1, df2)

        return mdf, ldf

    @staticmethod
    def get_features(df1, df2):
        fdf = em.get_features_for_matching(df1, df2, validate_inferred_attr_types=False)
        return fdf

    def build(self):
        df1, df2 = self.load()
        cdf = self.block(df1, df2)
        mdf, ldf = self.label(df1, df2, cdf)
        fdf = self.get_features(df1, df2)
        return df1, df2, cdf, mdf, ldf, fdf

    def rebuild(self):
        df1, df2 = self.load()
        fdf = self.get_features(df1, df2)
        return df1, df2, fdf

    def train_test(self, matcher, df1, df2, ldf, fdf):
        tdf_path = os.path.join(self.base_dir, self.dataset_dir, 'tdf.csv')
        rfdf_path = os.path.join(self.base_dir, self.dataset_dir, 'rfdf.csv')
        tfdf_path = os.path.join(self.base_dir, self.dataset_dir, 'tfdf.csv')
        if os.path.isfile(rfdf_path) and os.path.isfile(tfdf_path) and os.path.isfile(tdf_path):
            # Load training and testing data
            rfdf = em.read_csv_metadata(rfdf_path, ltable=df1, rtable=df2)
            tfdf = em.read_csv_metadata(tfdf_path, ltable=df1, rtable=df2)
        else:
            # Split into training and testing data
            tt_data = em.split_train_test(ldf, train_proportion=0.5, random_state=self.random_state)
            rdf = tt_data['train']
            tdf = tt_data['test']

            # Extract features
            print('Training data features ...')
            rfdf = em.extract_feature_vecs(rdf, feature_table=fdf, attrs_after=['gold'], show_progress=True)
            print('Testing data features ...')
            tfdf = em.extract_feature_vecs(tdf, feature_table=fdf, attrs_after=['gold'], show_progress=True)

            # Impute missing values
            rfdf.fillna(value=-1000000, inplace=True)
            tfdf.fillna(value=-1000000, inplace=True)

            # Save files
            em.to_csv_metadata(tdf, tdf_path, **{'encoding': 'latin-1'})
            em.to_csv_metadata(rfdf, rfdf_path)
            em.to_csv_metadata(tfdf, tfdf_path)

        # Training and Testing
        xattributes = ['_id', self.id1, self.id2, 'gold']
        matcher.fit(table=rfdf, exclude_attrs=xattributes, target_attr='gold')
        pdf = matcher.predict(
            table=tfdf, exclude_attrs=xattributes, target_attr='predict', probs_attr='mtch_prop',
            append=True, return_probs=True, inplace=False)

        # Evaluation
        evaluation = em.eval_matches(pdf, 'gold', 'predict')

        return rfdf, tfdf, pdf, evaluation

    def re_train_test(self, matcher, df1, df2, features):
        # Load training and testing data
        tdf_path = os.path.join(self.base_dir, self.dataset_dir, 'tdf.csv')
        rfdf_path = os.path.join(self.base_dir, self.dataset_dir, 'rfdf.csv')
        tfdf_path = os.path.join(self.base_dir, self.dataset_dir, 'tfdf.csv')
        tdf = em.read_csv_metadata(tdf_path, ltable=df1, rtable=df2)
        rfdf = em.read_csv_metadata(rfdf_path, ltable=df1, rtable=df2)
        tfdf = em.read_csv_metadata(tfdf_path, ltable=df1, rtable=df2)

        # Create a vectorizer to be used for textual explanations
        self.vectorizer = ERVectorizer(self.dname1, self.dname2, list(tdf)[3:-1], features)

        # Transform data into text
        ttxt = self.data_to_txt(tdf)

        # Training and Testing
        xattributes = ['_id', self.id1, self.id2, 'gold']
        matcher.fit(table=rfdf, exclude_attrs=xattributes, target_attr='gold')

        return ttxt, rfdf, tfdf

    def data_to_txt(self, df):
        return [row.to_json(orient='values', force_ascii=False)
                for idx, row in df.drop(['_id', self.id1, self.id2, 'gold'], axis=1).iterrows()]

    def cluster_predictions(self, predictions):
        attrs = [self.id1, self.id2, 'gold', 'predict']
        return {'tp': predictions[predictions.gold & predictions.predict][attrs],
                'tn': predictions[~predictions.gold & ~predictions.predict][attrs],
                'fp': predictions[~predictions.gold & predictions.predict][attrs],
                'fn': predictions[predictions.gold & ~predictions.predict][attrs]}

    def get_tuple_pairs_json(self, er_mdl):
        tp_path = os.path.join(self.base_dir, self.dataset_dir, 'tp.json')
        if not os.path.isfile(tp_path):
            t1_cols = [er_mdl.dataset.dname1 + '_' + c for c in list(er_mdl.df1)]
            t2_cols = [er_mdl.dataset.dname2 + '_' + c for c in list(er_mdl.df2)]
            pairs = []
            for pair in er_mdl.pdf.itertuples(index=False):
                id1, id2 = getattr(pair, er_mdl.dataset.id1), getattr(pair, er_mdl.dataset.id2)
                t1, t2 = er_mdl.df1.loc[id1], er_mdl.df2.loc[id2]
                if pair.predict:
                    status = 'True Positive' if pair.gold else 'False Positive'
                else:
                    status = 'False Negative' if pair.gold else 'True Negative'
                pairs.append([pair[0]] + t1.tolist() + t2.tolist() + [status])
            columns = ['_id'] + t1_cols + t2_cols + ['status']
            tp = {'columns': columns, 'data': pairs}
            tpf = open(tp_path, 'w')
            json.dump(tp, tpf)
        rel_path = './' + self.dataset_dir + 'tp.json'
        return rel_path

########################################################################################################################

# Datasets

class DBLPACMDataset(ERDataset):
    dataset_dir = 'static/datasets/ER/DBLP-ACM/'
    name = 'dblp_acm'
    dname1, dname2 = 'DBLP', 'ACM'
    id1, id2 = 'dblp_id', 'acm_id'

    def reblock(self, dblp_df, acm_df):
        print('Blocking ...')
        ob = em.OverlapBlocker()
        b1 = ob.block_tables(dblp_df, acm_df, 'title', 'title', overlap_size=2,
                             l_output_attrs=['title', 'authors', 'venue', 'year'],
                             r_output_attrs=['title', 'authors', 'venue', 'year'],
                             l_output_prefix='dblp_', r_output_prefix='acm_', n_jobs=self.n_jobs)
        b2 = ob.block_candset(b1, 'authors', 'authors', overlap_size=2, n_jobs=self.n_jobs)
        bdf = b2

        # Write dataframe to file to avoid re-blocking
        block_path = os.path.join(self.base_dir, self.dataset_dir, 'blocks.csv')
        em.to_csv_metadata(bdf, block_path, **{'encoding': 'latin-1'})

        return bdf


########################################################################################################################

class DBLPScholarDataset(ERDataset):
    dataset_dir = 'static/datasets/ER/DBLP-Scholar/'
    name = 'dblp_scholar'
    dname1, dname2 = 'DBLP', 'Scholar'
    id1, id2 = 'dblp_id', 'scholar_id'

    def reblock(self, dblp_df, scholar_df):
        print('Blocking ...')
        ob = em.OverlapBlocker()
        b1 = ob.block_tables(dblp_df, scholar_df, 'title', 'title', overlap_size=3,
                             l_output_attrs=['title', 'authors', 'venue', 'year'],
                             r_output_attrs=['title', 'authors', 'venue', 'year'],
                             l_output_prefix='dblp_', r_output_prefix='scholar_', n_jobs=self.n_jobs)
        b2 = ob.block_candset(b1, 'authors', 'authors', overlap_size=2, n_jobs=self.n_jobs)
        bdf = b2

        # Write dataframe to file to avoid re-blocking
        block_path = os.path.join(self.base_dir, self.dataset_dir, 'blocks.csv')
        em.to_csv_metadata(bdf, block_path, **{'encoding': 'latin-1'})

        return bdf

    @staticmethod
    def get_features(df1, df2):
        tok = em.get_tokenizers_for_matching()
        sim = em.get_sim_funs_for_matching()
        types1 = em.get_attr_types(df1)
        types2 = em.get_attr_types(df2)

        # Adjust attribute correspondences
        cor = em.get_attr_corres(df1, df2)
        cor['corres'] = [('title', 'title'),
                         ('authors', 'authors'),
                         ('venue', 'venue')]

        features = em.get_features(df1, df2, types1, types2, cor, tok, sim)

        # Add features
        # TODO: Add authors features
        pf = em.get_feature_fn('rel_diff(ltuple["year"], rtuple["year"])', tok, sim)
        em.add_feature(features, 'year_year_rel_diff', pf)

        return features


########################################################################################################################

class AbtBuyDataset(ERDataset):
    dataset_dir = 'static/datasets/ER/Abt-Buy/'
    name = 'abt_buy'
    dname1, dname2 = 'Abt', 'Buy'
    id1, id2 = 'abt_id', 'buy_id'

    def reblock(self, abt_df, buy_df):
        print('Blocking ...')
        ob = em.OverlapBlocker()
        b1 = ob.block_tables(abt_df, buy_df, 'name', 'name', overlap_size=3,
                             l_output_attrs=['name', 'description', 'price'],
                             r_output_attrs=['name', 'description', 'manufacturer', 'price'],
                             l_output_prefix='abt_', r_output_prefix='buy_', n_jobs=self.n_jobs)
        # b2 = ob.block_candset(b1, 'description', 'description',
        #                       overlap_size=3, allow_missing=True, n_jobs=self.n_jobs)
        bdf = b1

        # Write dataframe to file to avoid re-blocking
        block_path = os.path.join(self.base_dir, self.dataset_dir, 'blocks.csv')
        em.to_csv_metadata(bdf, block_path, **{'encoding': 'latin-1'})

        return bdf

    @staticmethod
    def get_features(df1, df2):
        tok = em.get_tokenizers_for_matching()
        sim = em.get_sim_funs_for_matching()
        types1 = em.get_attr_types(df1)
        types2 = em.get_attr_types(df2)

        # Adjust attribute correspondences
        cor = em.get_attr_corres(df1, df2)
        cor['corres'] = [('name', 'name'),
                         ('description', 'description')]

        features = em.get_features(df1, df2, types1, types2, cor, tok, sim)

        # Add features
        pf = em.get_feature_fn('rel_diff(ltuple["price"], rtuple["price"])', tok, sim)
        em.add_feature(features, 'price_price_rel_diff', pf)

        return features


########################################################################################################################

class AmazonGoogleDataset(ERDataset):
    dataset_dir = 'static/datasets/ER/Amazon-Google/'
    name = 'amazon_google'
    dname1, dname2 = 'Amazon', 'Google'
    id1, id2 = 'amazon_id', 'google_id'

    def reblock(self, amazon_df, google_df):
        print('Blocking ...')
        ob = em.OverlapBlocker()
        b1 = ob.block_tables(amazon_df, google_df, 'title', 'name', overlap_size=2, rem_stop_words=True,
                             l_output_attrs=['title', 'description', 'manufacturer', 'price'],
                             r_output_attrs=['name', 'description', 'manufacturer', 'price'],
                             l_output_prefix='amazon_', r_output_prefix='google_', n_jobs=self.n_jobs)
        # b2 = ob.block_candset(b1, 'description', 'description',
        #                       overlap_size=3, allow_missing=True, n_jobs=self.n_jobs)
        # b3 = ob.block_candset(b2, 'manufacturer', 'manufacturer',
        #                       overlap_size=1, allow_missing=True, n_jobs=self.n_jobs)
        bdf = b1

        # Write dataframe to file to avoid re-blocking
        block_path = os.path.join(self.base_dir, self.dataset_dir, 'blocks.csv')
        em.to_csv_metadata(bdf, block_path, **{'encoding': 'latin-1'})

        return bdf

    @staticmethod
    def get_features(df1, df2):
        tok = em.get_tokenizers_for_matching()
        sim = em.get_sim_funs_for_matching()
        types1 = em.get_attr_types(df1)
        types2 = em.get_attr_types(df2)

        # Adjust attribute correspondences
        cor = em.get_attr_corres(df1, df2)
        cor['corres'] = [('title', 'name'),
                         ('description', 'description'),
                         # ('manufacturer', 'manufacturer'),
                         ('price', 'price')]

        features = em.get_features(df1, df2, types1, types2, cor, tok, sim)

        # Add features
        # TODO: Inspect this feature. It seems to be not functioning
        pf = em.get_feature_fn('rel_diff(ltuple["price"], rtuple["price"])', tok, sim)
        em.add_feature(features, 'price_price_rel_diff', pf)

        return features


########################################################################################################################

class FodorsZagatDataset(ERDataset):
    dataset_dir = 'static/datasets/ER/Fodors-Zagat/'
    name = 'fodors_zagat'
    dname1, dname2 = 'Fodors', 'Zagat'
    id1, id2 = 'fodors_id', 'zagat_id'

    def reblock(self, fodors_df, zagat_df):
        print('Blocking ...')
        ob = em.OverlapBlocker()
        b1 = ob.block_tables(fodors_df, zagat_df, 'name', 'name', overlap_size=1, rem_stop_words=True,
                             l_output_attrs=['name', 'addr', 'city', 'phone', 'type'],
                             r_output_attrs=['name', 'addr', 'city', 'phone', 'type'],
                             l_output_prefix='fodors_', r_output_prefix='zagat_', n_jobs=self.n_jobs)
        # b2 = ob.block_candset(b1, 'addr', 'addr', overlap_size=2, allow_missing=True, n_jobs=self.n_jobs)
        # b3 = ob.block_candset(b2, 'city', 'city', overlap_size=1, allow_missing=True, n_jobs=self.n_jobs)
        bdf = b1

        # Write dataframe to file to avoid re-blocking
        block_path = os.path.join(self.base_dir, self.dataset_dir, 'blocks.csv')
        em.to_csv_metadata(bdf, block_path, **{'encoding': 'latin-1'})

        return bdf

    @staticmethod
    def get_features(df1, df2):
        tok = em.get_tokenizers_for_matching()
        sim = em.get_sim_funs_for_matching()
        types1 = em.get_attr_types(df1)
        types2 = em.get_attr_types(df2)

        # Adjust attribute correspondences
        cor = em.get_attr_corres(df1, df2)
        cor['corres'] = [('name', 'name'),
                         ('addr', 'addr'),
                         ('city', 'city'),
                         ('phone', 'phone'),
                         ('type', 'type')]

        features = em.get_features(df1, df2, types1, types2, cor, tok, sim)

        return features


########################################################################################################################
