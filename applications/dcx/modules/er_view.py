import plotly
from py_entitymatching.feature.autofeaturegen import _get_readable_feature_name


from gluon.html import *
from gluon.storage import Storage

from skater.core.explanations import Interpretation
from skater.model import InMemoryModel

import py_entitymatching as em
import matplotlib.pyplot as plt

import random

# Constants
RANDOM_STATE = 7

def show_dataset_matcher_form(dataset, matcher):
    slct_model = SELECT(
        OPTION('DBLP-ACM', _value='ds_dblp_acm', _selected=(dataset == 'ds_dblp_acm')),
        # OPTION('DBLP-Scholar', _value='ds_dblp_scholar', _selected = model=='ds_dblp_scholar'),
        # OPTION('Abt-Buy', _value='ds_abt_buy', _selected = model=='ds_abt_buy'),
        # OPTION('Amazon-Google', _value='ds_amazon_google', _selected=(dataset == 'ds_amazon_google')),
        OPTION('Fodors-Zagat', _value='ds_fodors_zagat', _selected=(dataset == 'ds_fodors_zagat')),
        _id='slct_dataset', _name='slct_dataset', _style='width:15%;')
    slct_matcher = SELECT(
        OPTION('RF', _value='mtchr_rf', _selected=(matcher == 'mtchr_rf')),
        OPTION('SVM', _value='mtchr_svm', _selected=(matcher == 'mtchr_svm')),
        _id='slct_matcher', _name='slct_matcher', _style='width:15%;')
    btn_run = BUTTON('Run', XML('&nbsp;&nbsp;'), I(_class='fas fa-play'),
                     _type='submit', _class='btn btn-success btn-sm')
    return FORM(LABEL('Dataset', XML('&nbsp;&nbsp;&nbsp;')), slct_model, XML('&nbsp;&nbsp;&nbsp;'),
                LABEL('Matcher', XML('&nbsp;&nbsp;&nbsp;')), slct_matcher, XML('&nbsp;&nbsp;&nbsp;'),
                btn_run,
                _id='frm_model')


def show_percentage_bar(value, text=''):
    if 0 < float(value) <= 25:
        clr = 'progress-bar-danger'
    elif 25 < float(value) <= 50:
        clr = 'progress-bar-warning'
    elif 50 < float(value) <= 90:
        clr = 'progress-bar-info'
    else:
        clr = 'progress-bar-success'
    return DIV(DIV(SPAN(value+'% '+text), _class='progress-bar '+clr, _role='progressbar', _style='width:'+value+'%',
                   **{'_aria-valuenow': value, '_aria-valuemin': 0, '_aria-valuemax': 100}),
               _class='progress', _style='margin: 10px;')


def show_stats_panel(er_mdl):
    return TABLE(
        TR(TD('Tuples in ', B(er_mdl.dataset.dname1)), TD("{:,}".format(er_mdl.n1))),
        TR(TD('Tuples in ', B(er_mdl.dataset.dname2)), TD("{:,}".format(er_mdl.n2))),
        TR(TD('Pairs '), TD("{:,}".format(er_mdl.n1 * er_mdl.n2))),
        TR(TD('Gold Matches'), TD("{:,}".format(er_mdl.gn))),
        TR(TD('Candidate Pairs '), TD("{:,}".format(er_mdl.cn))),
        TR(TD('Labeled Matches'), TD("{:,}".format(er_mdl.ln))),
        _width='100%')


def show_evaluation_panel(er_mdl):
    return TABLE(
        TR(TD('Precision'),
           TD(show_percentage_bar('{1:.{0}f}'.format(2, 100 * er_mdl.evaluation['precision']),
                                  '(' + str(int(er_mdl.evaluation['prec_numerator'])) + '/'
                                  + str(int(er_mdl.evaluation['prec_denominator'])) + ')'))),
        TR(TD('Recall'),
           TD(show_percentage_bar('{1:.{0}f}'.format(2, 100 * er_mdl.evaluation['recall']),
                                  '(' + str(int(er_mdl.evaluation['recall_numerator']))
                                  + '/' + str(int(er_mdl.evaluation['recall_denominator'])) + ')'))),
        TR(TD('F1'),
           TD(show_percentage_bar('{1:.{0}f}'.format(2, 100 * er_mdl.evaluation['f1'])))),
        TR(TD('False Positives'),
           TD(DIV(int(er_mdl.evaluation['false_pos_num']),
                  ' (out of ', int(er_mdl.evaluation['pred_pos_num']), ' positive predictions)',
                  _style='margin: 10px;'))),
        TR(TD('False Negatives'),
           TD(DIV(int(er_mdl.evaluation['false_neg_num']),
                  ' (out of ', int(er_mdl.evaluation['pred_neg_num']), ' negative predictions)',
                  _style='margin: 10px;'))),
        _width='100%', _border='0')


def show_original_tuple_pairs(er_mdl):
    t1_cols, t2_cols = list(er_mdl.df1), list(er_mdl.df2)
    thead = THEAD(TR(
        TH('_id'),
        CAT([TH(er_mdl.dataset.dname1 + '_' + c) for c in t1_cols]),
        CAT([TH(er_mdl.dataset.dname2 + '_' + c) for c in t2_cols]),
        TH('Prediction')))
    tbody = TBODY()  # Empty body, data will be loaded via ajax call
    return TABLE(thead, tbody, _id='tbl_tuple_pairs', _style='cursor: pointer; width: 100%;')


def show_predictions_plot(pdf, fws):
    # Dataframes
    # s = 100
    # predictions = Storage(tp=XML(results.predictions['tp'][:s].to_html(classes='tbl_predictions')),
    #                       tn=XML(results.predictions['tn'][:s].to_html(classes='tbl_predictions')),
    #                       fp=XML(results.predictions['fp'][:s].to_html(classes='tbl_predictions')),
    #                       fn=XML(results.predictions['fn'][:s].to_html(classes='tbl_predictions')))

    # Matplot
    # groups = predictions_df.groupby(['gold', 'predict'])
    # fig, ax = plt.subplots()
    # for name, group in groups:
    #     ax.scatter(group.ix[:, 3], group.ix[:, 4], label=name)
    # ax.legend(loc='best')
    # # fig = results.predictions_df.plot.scatter(3, 4).get_figure()
    #
    # tmp_file = BytesIO()
    # fig.savefig(tmp_file, format='png')
    # encoded = base64.b64encode(tmp_file.getvalue())
    # return CENTER(IMG(_src='data:image/png;base64,{}'.format(encoded)))

    # Plotly
    tpdf = pdf[(pdf.predict == True) & (pdf.gold == True)]
    tndf = pdf[(pdf.predict == False) & (pdf.gold == False)]
    fpdf = pdf[(pdf.predict == True) & (pdf.gold == False)]
    fndf = pdf[(pdf.predict == False) & (pdf.gold == True)]

    features = list(fws.index)
    xidx = 0
    yidx = 1
    xf = features[xidx]
    yf = features[yidx]

    data = [
        {'x': tpdf[xf], 'y': tpdf[yf], 'customdata': tpdf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'cross', 'color': 'darkgreen'}, 'name': 'True Positive'},
        {'x': tndf[xf], 'y': tndf[yf], 'customdata': tndf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'x', 'color': 'limegreen'}, 'name': 'True Negative'},
        {'x': fpdf[xf], 'y': fpdf[yf], 'customdata': fpdf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'cross', 'color': 'crimson'}, 'name': 'False Positive'},
        {'x': fndf[xf], 'y': fndf[yf], 'customdata': fndf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'x', 'color': 'salmon'}, 'name': 'False Negative'}
    ]

    annotations = [
        {'text': 'Feature Value', 'x': 0.5, 'y': -0.1, 'xref': 'paper', 'yref': 'paper', 'showarrow': False},
        {'text': 'Feature Value', 'x': 0, 'y': 1.15, 'xref': 'paper', 'yref': 'paper', 'showarrow': False},
    ]
    buttons_x = [{'label': f, 'args': [{'x': [tpdf[f], tndf[f], fpdf[f], fndf[f]]}]} for f in features]
    buttons_y = [{'label': f, 'args': [{'y': [tpdf[f], tndf[f], fpdf[f], fndf[f]]}]} for f in features]
    update_menus = [
        {'active': xidx, 'buttons': buttons_x, 'x': 0.5, 'y': -0.1, 'xanchor': 'center', 'direction': 'up'},
        {'active': yidx, 'buttons': buttons_y, 'x': -0.1, 'y': 1.1, 'xanchor': 'left'}
    ]
    layout = {
        'width': 1024,
        'height': 600,
        'hovermode': 'closest',
        'legend': {'x': 1.1, 'y': 1.1, 'xanchor': 'right', 'orientation': 'h'},
        'annotations': annotations,
        'updatemenus': update_menus
    }

    fig = {'data': data, 'layout': layout}
    plot = plotly.offline.plot(fig, show_link=False, output_type='div')
    return DIV(XML(plot), _id='div_plt_predictions')


def show_er_ui(er_mdl):
    stats = show_stats_panel(er_mdl)
    evaluation = show_evaluation_panel(er_mdl)
    tuples = show_original_tuple_pairs(er_mdl)
    predictions = show_predictions_plot(er_mdl.pdf, er_mdl.fws)
    return Storage(stats=stats, evaluation=evaluation, tuples=tuples, predictions=predictions)


def show_explanations_plot(xdf, fws, div_id):
    tpdf = xdf[(xdf.predict == True) & (xdf.gold == True)]
    tndf = xdf[(xdf.predict == False) & (xdf.gold == False)]
    fpdf = xdf[(xdf.predict == True) & (xdf.gold == False)]
    fndf = xdf[(xdf.predict == False) & (xdf.gold == True)]

    features = list(fws.index)
    xidx = 0
    yidx = 1
    xf = features[xidx]
    yf = features[yidx]

    data = [
        {'x': tpdf[xf], 'y': tpdf[yf], 'customdata': tpdf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'cross', 'color': 'darkgreen'}, 'name': 'True Positive'},
        {'x': tndf[xf], 'y': tndf[yf], 'customdata': tndf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'x', 'color': 'limegreen'}, 'name': 'True Negative'},
        {'x': fpdf[xf], 'y': fpdf[yf], 'customdata': fpdf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'cross', 'color': 'crimson'}, 'name': 'False Positive'},
        {'x': fndf[xf], 'y': fndf[yf], 'customdata': fndf['_id'], 'mode': 'markers', 'type': 'scattergl',
         'marker': {'symbol': 'x', 'color': 'salmon'}, 'name': 'False Negative'}
    ]

    annotations = [
        {'text': 'Feature Weight', 'x': 0.5, 'y': -0.1, 'xref': 'paper', 'yref': 'paper', 'showarrow': False},
        {'text': 'Feature Weight', 'x': 0, 'y': 1.15, 'xref': 'paper', 'yref': 'paper', 'showarrow': False}
    ]
    buttons_x = [{'label': f, 'args': [{'x': [tpdf[f], tndf[f], fpdf[f], fndf[f]]}]} for f in features]
    buttons_y = [{'label': f, 'args': [{'y': [tpdf[f], tndf[f], fpdf[f], fndf[f]]}]} for f in features]
    update_menus = [
        {'active': xidx, 'buttons': buttons_x, 'x': 0.5, 'y': -0.1, 'xanchor': 'center', 'direction': 'up'},
        {'active': yidx, 'buttons': buttons_y, 'x': -0.1, 'y': 1.1, 'xanchor': 'left'}
    ]
    layout = {
        'width': 1024,
        'height': 600,
        'hovermode': 'closest',
        'legend': {'x': 1.1, 'y': 1.1, 'xanchor': 'right', 'orientation': 'h'},
        # TODO: How could the range of axes be calculated to accommodate feature weights?
        # 'xaxis': {'range': [-0.2, 0.2]},
        'annotations': annotations,
        'updatemenus': update_menus
    }

    fig = {'data': data, 'layout': layout}
    plot = plotly.offline.plot(fig, show_link=False, output_type='div')
    return DIV(XML(plot), _id=div_id)


def show_explanations_tuple_pairs(er_mdl, sdf):
    t1_cols, t2_cols = list(er_mdl.df1), list(er_mdl.df2)
    thead = THEAD(TR(
        TH('_id'),
        CAT([TH(er_mdl.dataset.dname1 + '_' + c) for c in t1_cols]),
        CAT([TH(er_mdl.dataset.dname2 + '_' + c) for c in t2_cols]),
        TH('Prediction')))

    tbody = TBODY()
    for pair in sdf.itertuples(index=False):
        id1, id2 = getattr(pair, er_mdl.dataset.id1), getattr(pair, er_mdl.dataset.id2)
        t1, t2 = er_mdl.df1.loc[id1], er_mdl.df2.loc[id2]
        if pair.predict:
            status = 'True Positive' if pair.gold else 'False Positive'
        else:
            status = 'False Negative' if pair.gold else 'True Negative'
        tbody.append(TR(
            TD(pair[0]),
            CAT([TD(XML(t1[c])) for c in t1_cols]),
            CAT([TD(XML(t2[c])) for c in t2_cols]),
            TD(status)))

    return TABLE(thead, tbody, _id='tbl_rep_tuple_pairs', _style='cursor: pointer; width: 100%;')


def show_representatives_form():
    reps_form = FORM(
        LABEL('K Representatives'),
        XML('&nbsp;&nbsp;&nbsp;'),
        INPUT(_type='number', _value=20, _min=1, _max=100, _step=1, _name='nmbr_reps_k',
              _required='required', _style='width: 100px;'),
        XML('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'),
        LABEL('Filter'),
        XML('&nbsp;&nbsp;&nbsp;'),
        SELECT(
            OPTION('All', _value='a'),
            OPTGROUP(
                OPTION('True Positive', _value='tp'),
                OPTION('True Negative', _value='tn'),
                OPTION('False Positive', _value='fp'),
                OPTION('False Negative', _value='fn'),
                _label='Status'),
            OPTGROUP(
                OPTION('True', _value='t'),
                OPTION('False', _value='f'),
                OPTION('Duplicates', _value='pp'),
                OPTION('Non-duplicates', _value='pn'),
                _label='Predictions'
            ),
            OPTGROUP(
                OPTION('Duplicates', _value='gp'),
                OPTION('Non-duplicates', _value='gn'),
                _label='Ground Truth'
            ),
            _id='slct_reps_filter', _name='slct_reps_filter', _style='width: 200px;'),
        XML('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'),
        INPUT(_type='submit', _style='visibility: hidden;'),
        BUTTON(CAT('Reload', XML('&nbsp;'), I(_class='fas fa-lg fa-redo-alt')), _class='btn btn-info btn-sm'),
        _style='text-align: center;', _id='frm_rep_tuple_pairs')
    return reps_form


def show_features_table(fdf, fws):
    fwdf = fdf.assign(weight=[fws[fname] for i, fname in fdf['feature_name'].items()])
    fwdf = fwdf.sort_values('weight', ascending=False)

    f_thead = THEAD(TR(
        TH('Name'), TH('Left Attribute'), TH('Right Attribute'),
        TH('Tokenizer'), TH('Similarity Function'), TH('Importance')
    ))
    f_tbody = TBODY()
    for idx, row in fwdf.iterrows():
        name = row['feature_name']
        lattr = row['left_attribute']
        rattr = row['right_attribute']

        # Tokenizer
        tok = row['left_attr_tokenizer']
        try:
            tok = _get_readable_feature_name(tok) if tok else '-'
        except AssertionError:
            pass

        # Similarity Function
        sim = row['simfunction']
        try:
            sim = _get_readable_feature_name(sim)
        except AssertionError:
            pass

        # Weight
        w = row['weight']
        w = round(w, 2)
        # weight = round(w, 3)
        weight = DIV(DIV(SPAN(str(w) + '%'), _class='progress-bar progress-bar-info',
                         _role='progressbar', _style='width:' + str(w) + '%',
                         **{'_aria-valuenow': w, '_aria-valuemin': 0, '_aria-valuemax': 100}),
                     _class='progress', _style='margin: 10px;')

        f_tbody.append(TR(TD(name), TD(lattr), TD(rattr), TD(tok), TD(sim), TD(weight)))
    return TABLE(f_thead, f_tbody, _class='table table-striped table-sm', _id='tbl_features')


def show_explanations_ui(er_mdl):
    # Explanations Plot
    explanations = show_explanations_plot(er_mdl.xdf, er_mdl.fws, 'div_plt_explanations')

    # Sample Representatives Plot
    # representatives = show_explanations_plot(er_mdl.sdf, er_mdl.fws, 'div_plt_representatives')
    reps_form = show_representatives_form()
    reps_tbl = show_explanations_tuple_pairs(er_mdl, er_mdl.sdf)
    representatives = Storage(form=reps_form, table=reps_tbl)

    # Features
    features = show_features_table(er_mdl.fdf, er_mdl.fws)

    #Global explanation
    global_exp = show_global_exp(er_mdl)


    #Differential Analysis
    differential_analysis = show_differential_analysis(er_mdl)

    return Storage(explanations=explanations, representatives=representatives, features=features, global_exp=global_exp, differential_analysis=differential_analysis)

def show_global_exp(er_mdl):
    #From er_explain.py
    fl = list(er_mdl.rfdf)[3:-1]
    rfm = er_mdl.rfdf.as_matrix(columns=fl)
    rlm = er_mdl.rfdf.as_matrix(columns=['gold']).ravel().astype(int)
    X_train, y_train = rfm, rlm

    interpreter = Interpretation(X_train, feature_names=fl)
    model_inst = InMemoryModel(er_mdl.matcher.clf.predict, examples=X_train, model_type='classifier', unique_values=[0, 1], feature_names=fl, target_names=["NotDuplicate","Duplicate"])
    surrogate_explainer = interpreter.tree_surrogate(oracle=model_inst, seed=5)
    surrogate_explainer.fit(X_train, y_train, use_oracle=True, prune='pre', scorer_type='default')
    img_file_name = "global_exp_tree_skater_" + str(random.randint(0, 10000)) + ".png"
    surrogate_explainer.plot_global_decisions(file_name='applications/dcx/static/images/' + img_file_name)
    #global_explanation = surrogate_explainer.decisions_as_txt()
    global_explanation = IMG(_src="static/images/" + img_file_name, _width=900, _height=600)
    print(global_explanation)
    return global_explanation

def show_differential_analysis(er_mdl):
    #From er_explain.py
    fl = list(er_mdl.rfdf)[3:-1]
    #rfm = er_mdl.rfdf.as_matrix(columns=fl)
    rfm = er_mdl.rfdf.as_matrix(columns=fl)
    rlm = er_mdl.rfdf.as_matrix(columns=['gold']).ravel().astype(int)
    X_train, y_train = rfm, rlm

    models = {  "RF": em.RFMatcher(name='RF', random_state=RANDOM_STATE), 
                "SVM": em.SVMMatcher(name='SVM', random_state=RANDOM_STATE, probability=True),
                "LogReg": em.LogRegMatcher(name='LogReg', random_state=RANDOM_STATE),
                "DT": em.DTMatcher(name='DecisionTree', random_state=0)}

    for model_key in models:
        model = models[model_key]
        model.clf.fit(X_train, y_train)

    f, axes = plt.subplots(2,2, figsize = (14, 14))
    ax_dict = {
        'RF':axes[0][0],
        'SVM':axes[1][0],
        'LogReg':axes[0][1],
        'DT':axes[1][1]
    }

    interpreter = Interpretation(X_train, feature_names=fl)
    for model_key in models:
        if model_key == "RF" or model_key == "DT":
            pyint_model = InMemoryModel(models[model_key].clf.predict, examples=X_train, unique_values=models[model_key].clf.classes_)
        else:
            pyint_model = InMemoryModel(models[model_key].clf.predict_proba, examples=X_train, unique_values=models[model_key].clf.classes_)
        ax = ax_dict[model_key]
        interpreter.feature_importance.plot_feature_importance(pyint_model, ax=ax, ascending=True, progressbar=False)
        ax.set_title(model_key)
        ax.set_xlim(0.0, 1.0)
    
    img_file_name = "differential_analysis_" + str(random.randint(0, 10000)) + ".png"
    plt.savefig('applications/dcx/static/images/' + img_file_name)
    differential_analysis= IMG(_src="static/images/" + img_file_name, _width=1000, _height=700)
    
    return differential_analysis

def show_original_tuples(t1, t2):
    t1_cols = THEAD(TR([TH(idx) for idx, _ in t1.items()]))
    t1_vals = TBODY(TR([TD(val) for _, val in t1.items()]))
    t2_cols = THEAD(TR([TH(idx) for idx, _ in t2.items()]))
    t2_vals = TBODY(TR([TD(val) for _, val in t2.items()]))
    return TABLE(t1_cols, t1_vals, t2_cols, t2_vals, _class='table tabele-striped')


def show_local_explanation(explanations):
    tuples_div = DIV(DIV(show_original_tuples(explanations.t1, explanations.t2),
                         _class='panel-body'), _class='panel panel-default')
    lime_div = DIV(DIV(XML(explanations.lime.as_html(show_table=True)),
                       _class='panel-body'), _class='panel panel-default')
    # TODO: Inspect why anchor explanation messes up the outer page layout
    anchor_div = DIV(DIV(XML(explanations.anchor.as_html()),
                         _class='panel-body'), _class='panel panel-default')
    return DIV(tuples_div, lime_div, anchor_div)
