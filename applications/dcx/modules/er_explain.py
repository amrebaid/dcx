import os
from StringIO import StringIO

import numpy as np
import pandas as pd
from anchor.anchor_tabular import AnchorTabularExplainer
from lime.lime_tabular import LimeTabularExplainer


########################################################################################################################

# Vectorizer

class ERVectorizer:
    def __init__(self, lprefix, rprefix, columns, features):
        self.lprefix = lprefix.lower()
        self.rprefix = rprefix.lower()

        self.columns = columns
        self.lcolumns = [col[col.index('_') + 1:] for col in columns if col.startswith(self.lprefix)]
        self.rcolumns = [col[col.index('_') + 1:] for col in columns if col.startswith(self.rprefix)]

        self.features = features

    def transform(self, records):
        fdf = pd.DataFrame()

        for r in records:
            # Pair of tuples
            rdf = pd.read_csv(StringIO(r[1:-1]), header=None, names=self.columns)

            # Tuples
            lt = pd.Series({k: rdf.iloc[0][self.lprefix + '_' + k] for k in self.lcolumns})
            rt = pd.Series({k: rdf.iloc[0][self.rprefix + '_' + k] for k in self.rcolumns})

            # Features
            # The external function call gives an exception sometimes,
            # so I had to go one deeper level and call the functions myself
            # fdf = fdf.append(apply_feat_fns(lt, rt, self.features), ignore_index=True)
            f_names = list(self.features['feature_name'])
            f_funcs = list(self.features['function'])
            f_vals = []
            for f in f_funcs:
                try:
                    f_vals.append(f(lt, rt))
                except TypeError:
                    f_vals.append(-1000000)
            f_vector = dict(zip(f_names, f_vals))
            fdf = fdf.append(f_vector, ignore_index=True)

        return fdf.fillna(value=-1000000)

    def fit(self):
        pass


########################################################################################################################

# LIME

class LIMEExplainer:
    def __init__(self):
        self.explanations_dir = 'static/explanations/lime/'

    @staticmethod
    def explain_prediction(er_mdl, pair_id):
        pair_idx = er_mdl.tfdf.index[er_mdl.tfdf['_id'] == pair_id][0]

        # Remove ids and gold from features
        fl = list(er_mdl.rfdf)[3:-1]
        rfm = er_mdl.rfdf.as_matrix(columns=fl)
        tfm = er_mdl.tfdf.as_matrix(columns=fl)

        # Explainers
        classes = ['non-match', 'match']
        tbl_explainer = LimeTabularExplainer(
            rfm, feature_names=fl, verbose=False, class_names=classes, discretize_continuous=False)
        # txt_explainer = lime.lime_text.LimeTextExplainer(class_names=classes)

        # Explanations
        # Tabular
        tbl_exp = tbl_explainer.explain_instance(
            tfm[pair_idx], er_mdl.matcher.clf.predict_proba, top_labels=1, num_features=len(fl))
        # Textual
        # pipeline = sklearn.pipeline.make_pipeline(model.vectorizer, matcher.clf)
        # txt_exp = txt_explainer.explain_instance(
        #     ttxt[pair_idx].decode('utf-8'), pipeline.predict_proba, num_features=10, num_samples=100)

        return tbl_exp

    def explain_predictions(self, er_mdl):
        # Remove ids and gold from features
        fl = list(er_mdl.rfdf)[3:-1]
        # fl = er_mdl.fdf['feature_name'].tolist()
        rfm = er_mdl.rfdf[fl].values
        tfm = er_mdl.tfdf[fl].values

        # Explainer
        explainer = LimeTabularExplainer(rfm, feature_names=fl, class_names=['non-match', 'match'])

        # Explanations Dataframe
        xdf_path = os.path.join(er_mdl.base_dir, self.explanations_dir,
                                'xdf_' + er_mdl.dataset.name + '_' + er_mdl.matcher.get_name() + '.csv')

        if os.path.isfile(xdf_path):
            # Load explanations
            xdf = pd.read_csv(xdf_path)
        else:
            # Generate all explanations
            pc = len(er_mdl.predictions_df.index)
            print('Explaining ' + str(pc) + ' predictions ...')
            explanations = []
            for i in range(pc):
                if i > 0 and i % 100 == 0:
                    print('Prediction #' + str(i))
                row = er_mdl.predictions_df.iloc[i]
                explanation = explainer.explain_instance(
                    tfm[i], er_mdl.matcher.clf.predict_proba, top_labels=1, num_features=len(fl))
                em = explanation.as_map()
                ep = explanation.predict_proba[1]
                label = explanation.available_labels()[0]
                feature_weights = em[label]
                labeled_weights = {fl[fw[0]]: fw[1] for fw in feature_weights}
                labeled_weights['_id'] = row['_id']
                labeled_weights[er_mdl.dataset.id1] = row[er_mdl.dataset.id1]
                labeled_weights[er_mdl.dataset.id2] = row[er_mdl.dataset.id2]
                labeled_weights['gold'] = row['gold']
                labeled_weights['predict'] = row['predict']
                labeled_weights['mtch_prop'] = row['mtch_prop']
                labeled_weights['exp_prop'] = ep
                explanations.append(labeled_weights)
            cols = list(er_mdl.predictions_df) + ['exp_prop']
            xdf = pd.DataFrame(data=explanations, columns=cols)

            # Save to file
            xdf.to_csv(xdf_path, index=False)

        return xdf

    @staticmethod
    def get_representative_explanations(rfdf, xdf, k, fltr):
        # Filter explanations dataframe
        # if fltr == 'a':
        fxdf = xdf
        if fltr == 'tp':
            fxdf = xdf[(xdf.predict == True) & (xdf.gold == True)]
        elif fltr == 'tn':
            fxdf = xdf[(xdf.predict == False) & (xdf.gold == False)]
        elif fltr == 'fp':
            fxdf = xdf[(xdf.predict == True) & (xdf.gold == False)]
        elif fltr == 'fn':
            fxdf = xdf[(xdf.predict == False) & (xdf.gold == True)]
        elif fltr == 't':
            fxdf = xdf[((xdf.predict == True) & (xdf.gold == True)) | ((xdf.predict == False) & (xdf.gold == False))]
        elif fltr == 'f':
            fxdf = xdf[((xdf.predict == True) & (xdf.gold == False)) | ((xdf.predict == False) & (xdf.gold == True))]
        elif fltr == 'pp':
            fxdf = xdf[xdf.predict == True]
        elif fltr == 'pn':
            fxdf = xdf[xdf.predict == False]
        elif fltr == 'gp':
            fxdf = xdf[xdf.gold == True]
        elif fltr == 'gn':
            fxdf = xdf[xdf.gold == False]

        # Remove ids and gold from features
        offset = 3
        fl = list(rfdf)[offset:-1]

        ###
        # Code is copied and modified from submodular_pick in lime
        ###

        # Create the n x d dimensional 'explanation matrix' w
        n = fxdf.shape[0]
        d = len(fl)
        w = np.zeros((n, d))
        for i in range(n):
            exp = fxdf.iloc[i]
            for j in range(d):
                w[i, j] = exp.iloc[j + offset]

        # Create the global importance vector I_j described in the paper
        importance = np.sum(abs(w), axis=0) ** .5

        # Now run the SP-LIME greedy algorithm
        remaining_indices = set(range(n))
        v = []
        for _ in range(min(k, n)):
            best = 0
            best_ind = None
            for i in remaining_indices:
                current = np.dot(
                    (np.sum(abs(w)[v + [i]], axis=0) > 0), importance
                )  # coverage function
                if current >= best:
                    best = current
                    best_ind = i
            v.append(best_ind)
            remaining_indices -= {best_ind}

        ###
        # End of copied code
        ###

        sdf = fxdf.iloc[v]
        return sdf

    @staticmethod
    def analyze_features(matcher, rfdf, tfdf, predictions_df):
        # Remove ids and gold from features
        fl = list(rfdf)[3:-1]
        rfm = rfdf.as_matrix(columns=fl)
        tfm = tfdf.as_matrix(columns=fl)

        # Explainer
        explainer = LimeTabularExplainer(rfm, feature_names=fl, class_names=['non-match', 'match'])

        # Calculate contributions for each feature
        contributions = []
        # TODO: analyze features for all data not only some records, within a reasonable time
        for i in range(0):
            row = predictions_df.iloc[i]
            explanation = explainer.explain_instance(tfm[i], matcher.clf.predict_proba, top_labels=1, num_features=5)

            em = explanation.as_map()
            fc = em[0] if 0 in em else em[1]
            contributions.append((fc, row['gold'], row['predict']))
        return contributions

    @staticmethod
    def get_feature_weights(xdf):
        xfdf = xdf.iloc[:, 3:-4]
        # TODO: How to calculate this??
        fws = xfdf.abs().mean().sort_values(ascending=False)
        fws = fws * 100 / fws.sum()
        return fws


########################################################################################################################

# Anchor

class AnchorExplainer:
    def __init__(self):
        pass

    @staticmethod
    def explain_prediction(er_mdl, pair_id):
        # Pair index in dataframe
        pair_idx = er_mdl.tfdf.index[er_mdl.tfdf['_id'] == pair_id][0]

        # Reformat data
        fl = list(er_mdl.rfdf)[3:-1]
        rfm = er_mdl.rfdf.as_matrix(columns=fl)
        tfm = er_mdl.tfdf.as_matrix(columns=fl)
        dfm = np.concatenate((rfm, tfm))
        rlm = er_mdl.rfdf.as_matrix(columns=['gold']).ravel().astype(int)
        tlm = er_mdl.tfdf.as_matrix(columns=['gold']).ravel().astype(int)

        # Fit explainer and matcher
        classes = ['non-match', 'match']
        explainer = AnchorTabularExplainer(class_names=classes, feature_names=fl, data=dfm, categorical_names={})
        # split = int(len(rlm) * .8)
        # explainer.fit(train_data=rfm[:split], train_labels=rlm[:split],
        #               validation_data=rfm[split:], validation_labels=rlm[split:])
        explainer.fit(train_data=rfm, train_labels=rlm, validation_data=tfm, validation_labels=tlm)
        er_mdl.matcher.clf.fit(explainer.encoder.transform(rfm), rlm)

        # Explanation
        return explainer.explain_instance(tfm[pair_idx], er_mdl.matcher.clf.predict)


########################################################################################################################
