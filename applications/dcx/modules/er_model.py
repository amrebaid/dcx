import logging

from er_datasets import *
from er_explain import *
from gluon.storage import Storage

# Skip warnings
logging.getLogger().setLevel(logging.ERROR)

# Constants
RANDOM_STATE = 7
N_JOBS = 1


def get_dataset(ds, fldr):
    if ds == 'ds_dblp_acm':
        return DBLPACMDataset(base_dir=fldr, random_state=RANDOM_STATE, n_jobs=N_JOBS)
    elif ds == 'ds_dblp_scholar':
        return DBLPScholarDataset(base_dir=fldr, random_state=RANDOM_STATE, n_jobs=N_JOBS)
    elif ds == 'ds_abt_buy':
        return AbtBuyDataset(base_dir=fldr, random_state=RANDOM_STATE, n_jobs=N_JOBS)
    elif ds == 'ds_amazon_google':
        return AmazonGoogleDataset(base_dir=fldr, random_state=RANDOM_STATE, n_jobs=N_JOBS)
    elif ds == 'ds_fodors_zagat':
        return FodorsZagatDataset(base_dir=fldr, random_state=RANDOM_STATE, n_jobs=N_JOBS)


def get_matcher(mtchr):
    if mtchr == 'mtchr_rf':
        return em.RFMatcher(name='RF', random_state=RANDOM_STATE)
    elif mtchr == 'mtchr_svm':
        return em.SVMMatcher(name='SVM', random_state=RANDOM_STATE, probability=True)


def run_model(ds, mtchr, fldr, k=20, fltr='a'):
    er_mdl = Storage(base_dir=fldr, dataset=None, matcher=None,
                     dname1=None, dname2=None, n1=0, n2=0, cn=0, gn=0, ln=0,
                     fdf=None, rfdf=None, tfdf=None, pdf=None, evaluation=None,
                     tpjson=None, xdf=None, sdf=None, fws=None)

    # Dataset and Matcher
    dataset = get_dataset(ds, fldr)
    matcher = get_matcher(mtchr)
    er_mdl.dataset = dataset
    er_mdl.matcher = matcher

    # Build, train and test model
    df1, df2, cdf, mdf, ldf, fdf = dataset.build()
    rfdf, tfdf, pdf, evaluation = dataset.train_test(matcher, df1, df2, ldf, fdf)

    er_mdl.n1, er_mdl.n2 = len(df1), len(df2)
    er_mdl.df1, er_mdl.df2 = df1, df2
    er_mdl.cn, er_mdl.gn, er_mdl.ln = len(cdf), len(mdf), ldf['gold'].value_counts()[1]
    er_mdl.fdf = fdf
    er_mdl.rfdf, er_mdl.tfdf, er_mdl.pdf, er_mdl.evaluation = rfdf, tfdf, pdf, evaluation

    # Original tuple pairs
    er_mdl.tpjson = dataset.get_tuple_pairs_json(er_mdl)

    # Explanations and Feature Weights
    explainer = LIMEExplainer()
    xdf = explainer.explain_predictions(er_mdl)
    sdf = explainer.get_representative_explanations(rfdf, xdf, k, fltr)
    fws = explainer.get_feature_weights(xdf)

    er_mdl.xdf, er_mdl.sdf, er_mdl.fws = xdf, sdf, fws

    # Return ER Model
    return er_mdl


def explain_prediction(ds, mtchr, pair_id, fldr):
    # Restore dataset and matcher
    dataset = get_dataset(ds, fldr)
    matcher = get_matcher(mtchr)

    # Retrain model
    df1, df2, fdf = dataset.rebuild()
    ttxt, rfdf, tfdf = dataset.re_train_test(matcher, df1, df2, fdf)

    er_mdl = Storage(dataset=dataset, matcher=matcher, df1=df1, df2=df2, rfdf=rfdf, tfdf=tfdf, ttxt=ttxt)

    # Original Tuples
    pair_idx = tfdf.index[er_mdl.tfdf['_id'] == pair_id][0]
    pair = tfdf.iloc[pair_idx]
    id1, id2 = pair[dataset.id1],  pair[dataset.id2]
    t1, t2 = df1.loc[id1], df2.loc[id2]

    # Explain
    return Storage(t1=t1, t2=t2,
                   lime=LIMEExplainer().explain_prediction(er_mdl, pair_id),
                   anchor=AnchorExplainer().explain_prediction(er_mdl, pair_id))
