import er_model
import er_view


def index():
    # Read parameters
    dataset = request.vars['slct_dataset'] if 'slct_dataset' in request.vars else None
    matcher = request.vars['slct_matcher'] if 'slct_matcher' in request.vars else None

    frm_dataset_matcher = er_view.show_dataset_matcher_form(dataset, matcher)

    if dataset and matcher:
        # Store to session
        session.dataset = dataset
        session.matcher = matcher

        # Run ER
        er_mdl = er_model.run_model(dataset, matcher, request.folder)
        er_ui = er_view.show_er_ui(er_mdl)
        exps_ui = er_view.show_explanations_ui(er_mdl)
        return dict(frm_dataset_matcher=frm_dataset_matcher, er_mdl=er_mdl, er_ui=er_ui, exps_ui=exps_ui)
    else:
        return dict(frm_dataset_matcher=frm_dataset_matcher, er_ui=None)


def explain():
    # Read parameters
    pair_id = int(request.vars['pair_id'])
    dataset = session.dataset
    matcher = session.matcher

    # Show local explanations
    local_explanations = er_model.explain_prediction(dataset, matcher, pair_id, request.folder)
    local_explanations_ui = er_view.show_local_explanation(local_explanations)
    return dict(local_explanations_ui=local_explanations_ui)


def reload_reps():
    # Read parameters
    dataset = session.dataset
    matcher = session.matcher

    # Reload representatives
    k = int(request.vars['nmbr_reps_k']) if request.vars and 'nmbr_reps_k' in request.vars else 20
    fltr = request.vars['slct_reps_filter'] if request.vars and 'slct_reps_filter' in request.vars else 'a'
    er_mdl = er_model.run_model(dataset, matcher, request.folder, k=k, fltr=fltr)
    reps_tbl = er_view.show_explanations_tuple_pairs(er_mdl, er_mdl.sdf)
    return reps_tbl


def playground():
    # dataset = DBLPACMDataset(base_dir=request.folder, random_state=7, n_jobs=1)
    # matcher = em.RFMatcher(name='RF', random_state=7)
    #
    # df1, df2, fdf = dataset.rebuild()
    # ttxt, rfdf, tfdf = dataset.re_train_test(matcher, df1, df2, fdf)
    #
    # fl = list(rfdf)[3:-1]
    # rfm = rfdf.as_matrix(columns=fl)
    # tfm = tfdf.as_matrix(columns=fl)
    #
    # dfm = np.concatenate((rfm, tfm))
    # rlm = rfdf.as_matrix(columns=['gold']).astype(int).ravel()
    # tlm = tfdf.as_matrix(columns=['gold']).astype(int).ravel()
    #
    # classes = ['non-match', 'match']
    # explainer = AnchorTabularExplainer(class_names=classes, feature_names=fl, data=dfm, categorical_names={})
    # explainer.fit(train_data=rfm, train_labels=rlm, validation_data=tfm, validation_labels=tlm)
    # matcher.clf.fit(explainer.encoder.transform(rfm), rlm)
    #
    # # matcher.clf.fit(explainer.encoder.transform(rfm), rlm)
    # exp = explainer.explain_instance(tfm[12], matcher.clf.predict, threshold=0.95)
    # return exp.as_html()
    pass
