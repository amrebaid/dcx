## ExplainER


## Installation Instructions

    pip install jupyter
    pip install plotly
    pip install -U numpy scipy py_entitymatching
    pip install lime
    pip install anchor_exp

To start web2py there is NO NEED to install it. Just unzip and do:

    python web2py.py

## Docker Installation Instructions
We also have provided a Dockerfile to easily run it. If you want to go this route, run the following commands.

	docker build --tag=dcx .
	docker run -p 4000:8000 dcx

Access ExplainER at http://0.0.0.0:4000


## Issues?

## License

MIT License

Copyright (c) 2019 ExplainER

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
